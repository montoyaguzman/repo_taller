// var url = 'https://services.odata.org/V4/Northwind/Northwind.svc'
var productosObtenidos

function getProducts() {

  var url = 'https://services.odata.org/V4/Northwind/Northwind.svc/Products'
  var request = new XMLHttpRequest();

  //request.onreadystatechange = (entrada) => {
  request.onreadystatechange = function() {
      //console.log('entrada', entrada)
      if(request.readyState == 4 && request.status == 200) {
        // console.table(JSON.parse(request.responseText).value)
        productosObtenidos = request.responseText
        procesaProductos()
      }
  }
  request.open('GET', url, true)
  request.send(null)

}

function procesaProductos() {
  var JSONProductos = JSON.parse(productosObtenidos)
  var divTable = productsTable

  var tabla = document.createElement('table')
  var tbody = document.createElement('tbody')
  tabla.classList.add('table')
  tabla.classList.add('table-striped')


  for (var i = 0; i < JSONProductos.value.length; i++) {

    var newRow = document.createElement('tr')

    var column1 = document.createElement('td')
    column1.innerText = JSONProductos.value[i].ProductName

    var column2 = document.createElement('td')
    column2.innerText = JSONProductos.value[i].UnitPrice

    var column3 = document.createElement('td')
    column3.innerText = JSONProductos.value[i].UnitsInStock

    newRow.appendChild(column1)
    newRow.appendChild(column2)
    newRow.appendChild(column1)

    tbody.appendChild(newRow)



  }

  tabla.appendChild(tbody)
  divTable.appendChild(tabla)


}
