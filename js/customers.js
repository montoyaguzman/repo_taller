// import * as constantes from './properties.js';

var customersObtenidos

function getCustomers() {

  // console.log('AQUI', constantes.BASE_URL)

  var url = 'https://services.odata.org/V4/Northwind/Northwind.svc/Customers'
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
          // console.table(JSON.parse(request.responseText).value)
          customersObtenidos = request.responseText
          procesaClientes()
      }

  }

  request.open('GET', url, true)
  request.send()

}


function procesaClientes() {
  var JSONClientes = JSON.parse(customersObtenidos)
  var divTableCustomer = customersTable

  var tabla = document.createElement('table')
  var tbody = document.createElement('tbody')
  tabla.classList.add('table')
  tabla.classList.add('table-striped')


  for (var i = 0; i < JSONClientes.value.length; i++) {

    var newRow = document.createElement('tr')
    var customer = JSONClientes.value[i]

    var column1 = document.createElement('td')
    var flagImg = document.createElement('img')
    flagImg.classList.add('flag')
    
    let stringFlag = ''
    if(customer.Country === 'UK') {
      stringFlag = 'United-Kingdom'
    } else {
      stringFlag = customer.Country
    }

    flagImg.src = `https://www.countries-ofthe-world.com/flags-normal/flag-of-${stringFlag}.png`
    column1.appendChild(flagImg)

    var column2 = document.createElement('td')
    column2.innerText = customer.ContactName

    var column3 = document.createElement('td')
    column3.innerText = customer.City

    newRow.appendChild(column1)
    newRow.appendChild(column2)
    newRow.appendChild(column1)

    tbody.appendChild(newRow)



  }

  tabla.appendChild(tbody)
  divTableCustomer.appendChild(tabla)
}
